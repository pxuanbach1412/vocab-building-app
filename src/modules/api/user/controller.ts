import {
  Body,
  ClassSerializerInterceptor,
  Controller,
  Delete,
  Get,
  Param,
  Patch,
  Post,
  Put,
  Req,
  UseGuards,
  UseInterceptors,
} from '@nestjs/common';
import UserService from './service';
import { ApiTags } from '@nestjs/swagger';
import { JwtAuthGuard } from '../auth/jwtAuth.guard';
import RequestWithUser from '../auth/requestUser.interface';
import { User } from './schema';
import MongooseClassSerializerInterceptor from 'src/utils/mongooseClassSerializer.interceptor';
import { ApiBearerAuth } from '@nestjs/swagger';
import UpdateUserDto from './dto/update.dto';

@UseInterceptors(MongooseClassSerializerInterceptor(User))
@Controller('users')
@ApiTags('users')
export default class UserController {
  constructor(private readonly service: UserService) {}

  @UseGuards(JwtAuthGuard)
  @Get('me')
  @ApiBearerAuth('access-token')
  getMe(@Req() request: RequestWithUser) {
    return request.user;
  }

  // @Get(':id')
  // async getById(@Param() { id }: ParamsMongoId) {
  //     return this.service.findOne(id)
  // }

  // @Post()
  // async createPost(@Body() payload: VocabularyDto) {
  //     return this.service.create(payload);
  // }

  // @Delete(':id')
  // async deletePost(@Param() { id }: ParamsMongoId) {
  //     return this.service.delete(id);
  // }

  @UseGuards(JwtAuthGuard)
  @Patch('me')
  @ApiBearerAuth('access-token')
  async updateUser(
    @Req() request: RequestWithUser,
    @Body() payload: UpdateUserDto,
  ) {
    return await this.service.update(request.user._id.toString(), payload);
  }
}
