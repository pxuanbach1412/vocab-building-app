import {
  Body,
  Controller,
  Delete,
  Get,
  Param,
  Post,
  Put,
} from '@nestjs/common';
import VocabularyService from './service';
import CreateVocabularyDto from './dto/create.dto';
import UpdateVocabolaryDto from './dto/update.dto';
import ParamsMongoId from '../../../utils/paramsWithId';
import { ApiTags, ApiParam } from '@nestjs/swagger';

@Controller('vocabularies')
@ApiTags('vocabularies')
export default class VocabularyController {
  constructor(private readonly service: VocabularyService) {}

  @Get()
  async getAll() {
    return this.service.getAll();
  }

  @ApiParam({ name: 'id', required: true })
  @Get(':id')
  async getById(@Param() { id }: ParamsMongoId) {
    return this.service.getOne(id);
  }

  @Post()
  async create(@Body() payload: CreateVocabularyDto) {
    return this.service.create(payload);
  }

  @ApiParam({ name: 'id', required: true })
  @Delete(':id')
  async delete(@Param() { id }: ParamsMongoId) {
    return this.service.delete(id);
  }

  @ApiParam({ name: 'id', required: true })
  @Put(':id')
  async update(
    @Param() { id }: ParamsMongoId,
    @Body() payload: UpdateVocabolaryDto,
  ) {
    return this.service.update(id, payload);
  }
}
