import { Model } from 'mongoose';
import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Vocabulary, VocabularyDocument } from './schema';
import { NotFoundException } from '@nestjs/common';
import CreateVocabularyDto from './dto/create.dto';
import UpdateVocabolaryDto from './dto/update.dto';

@Injectable()
class VocabularyService {
  constructor(
    @InjectModel(Vocabulary.name) private model: Model<VocabularyDocument>,
  ) {}

  async getAll() {
    return this.model.find();
  }

  async getOne(id: string) {
    const result = await this.model.findById(id);
    if (!result) {
      throw new NotFoundException();
    }
    return result;
  }

  create(data: CreateVocabularyDto) {
    const result = new this.model(data);
    return result.save();
  }

  async update(id: string, data: UpdateVocabolaryDto) {
    const result = await this.model
      .findByIdAndUpdate(id, data)
      .setOptions({ overwrite: true, new: true });
    if (!result) {
      throw new NotFoundException();
    }
    return result;
  }

  async delete(id: string) {
    const result = await this.model.findByIdAndDelete(id);
    if (!result) {
      throw new NotFoundException();
    }
  }
}

export default VocabularyService;
