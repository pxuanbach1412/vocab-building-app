import {
  IsString,
  IsNotEmpty,
  IsArray,
  IsNumber,
  IsMongoId,
} from 'class-validator';
import { ApiProperty, ApiPropertyOptional } from '@nestjs/swagger';
import ExampleDto from './example.dto';

class CreateVocabularyDto {
  @ApiProperty()
  @IsString()
  @IsNotEmpty()
  vocabCollection: string;

  @ApiProperty()
  @IsString()
  @IsNotEmpty()
  word: string;

  @ApiProperty()
  @IsString()
  @IsNotEmpty()
  pronunciation: string;

  @ApiProperty()
  @IsString()
  @IsNotEmpty()
  shortMean: string;

  @ApiPropertyOptional()
  @IsString()
  longMean: string;

  @ApiPropertyOptional({ type: [ExampleDto] })
  @IsArray()
  examples: ExampleDto[];
}

export default CreateVocabularyDto;
