import {
  IsString,
  IsNotEmpty,
  IsArray,
  IsNumber,
  IsMongoId,
} from 'class-validator';
import { ApiProperty, ApiPropertyOptional } from '@nestjs/swagger';
import ExampleDto from './example.dto';

class UpdateVocabolaryDto {
  @ApiPropertyOptional()
  @IsString()
  vocabCollection?: string;

  @ApiPropertyOptional()
  @IsString()
  word?: string;

  @ApiPropertyOptional()
  @IsString()
  pronunciation?: string;

  @ApiPropertyOptional()
  @IsString()
  shortMean?: string;

  @ApiPropertyOptional()
  @IsString()
  longMean?: string;

  @ApiPropertyOptional({ type: [ExampleDto] })
  @IsArray()
  examples?: ExampleDto[];
}

export default UpdateVocabolaryDto;
