import { IsString, IsNumber } from 'class-validator';
import { ApiProperty } from '@nestjs/swagger';

class ExampleDto {
  @ApiProperty()
  @IsNumber()
  order: number;

  @ApiProperty()
  @IsString()
  sentence: string;

  @ApiProperty()
  @IsString()
  mean: string;
}

export default ExampleDto;
