import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import {
  HydratedDocument,
  ObjectId,
  now,
  Schema as mongooseSchema,
} from 'mongoose';
import { Transform, Type } from 'class-transformer';
import { VocabCollection } from '../vocabCollection/schema';
import { Field, ObjectType } from '@nestjs/graphql';

export type VocabularyDocument = HydratedDocument<Vocabulary>;

@ObjectType()
@Schema()
export class Example {
  @Field()
  @Prop({ type: Number, default: 0 })
  order: number;

  @Field()
  @Prop({ type: String })
  sentence: string;

  @Field()
  @Prop({ type: String })
  mean: string;
}

export const ExampleSchema = SchemaFactory.createForClass(Example);

@ObjectType()
@Schema({ timestamps: true })
export class Vocabulary {
  @Field(() => String)
  @Transform(({ value }) => value.toString())
  @Type(() => String)
  _id: ObjectId;

  @Field((type) => VocabCollection)
  @Prop({ type: mongooseSchema.Types.ObjectId, ref: VocabCollection.name })
  @Type(() => VocabCollection)
  vocabCollection: VocabCollection;

  @Field()
  @Prop({ type: String, required: true })
  word: string;

  @Field()
  @Prop({ type: String, required: true })
  wordType: string;

  @Field({ nullable: true })
  @Prop({ type: String })
  pronunciation: string;

  @Field()
  @Prop({ type: String, required: true })
  shortMean: string;

  @Field({ nullable: true })
  @Prop({ type: String })
  longMean: string;

  @Field((type) => [Example], { nullable: true })
  @Prop({ _id: false, type: [ExampleSchema], default: [] })
  examples: Example[];

  @Field()
  @Prop({ default: now() })
  createdAt: Date;

  @Field()
  @Prop({ default: now() })
  updatedAt: Date;
}

export const VocabularySchema = SchemaFactory.createForClass(Vocabulary);
