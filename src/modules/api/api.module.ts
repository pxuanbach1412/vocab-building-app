import { Module } from '@nestjs/common';
import { VocabularyModule } from './vocabulary/module';
import { UserModule } from './user/module';
import { AuthModule } from './auth/module';
import { VocabCollectionModule } from './vocabCollection/module';
import { RoleModule } from './role/module';
import { UserVocabModule } from './userVocab/module';

@Module({
  imports: [
    AuthModule,
    RoleModule,
    UserModule,
    VocabCollectionModule,
    VocabularyModule,
    UserVocabModule,
  ],
  controllers: [],
  providers: [],
  exports: [],
})
export class ApiModule {}
