import {
  IsString,
  IsNotEmpty,
  IsArray,
  IsNumber,
  IsMongoId,
} from 'class-validator';
import { ApiProperty, ApiPropertyOptional } from '@nestjs/swagger';

class CreateUserVocabDto {
  @ApiProperty()
  @IsString()
  @IsNotEmpty()
  vocabulary: string;

  @ApiProperty()
  @IsString()
  @IsNotEmpty()
  user: string;

  @ApiPropertyOptional()
  @IsString()
  @IsNotEmpty()
  remembered: boolean;
}

export default CreateUserVocabDto;
