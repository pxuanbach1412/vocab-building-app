import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { UserVocab, UserVocabSchema } from './schema';
import UserVocabService from './service';

@Module({
  imports: [
    MongooseModule.forFeature([
      { name: UserVocab.name, schema: UserVocabSchema },
    ]),
  ],
  controllers: [],
  providers: [UserVocabService],
})
export class UserVocabModule {}
