import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import {
  HydratedDocument,
  ObjectId,
  now,
  Schema as mongooseSchema,
} from 'mongoose';
import { Transform, Type } from 'class-transformer';
import { Vocabulary } from '../vocabulary/schema';
import { User } from '../user/schema';

export type UserVocabDocument = HydratedDocument<UserVocab>;

@Schema({ timestamps: true })
export class UserVocab {
  @Transform(({ value }) => value.toString())
  _id: ObjectId;

  @Prop({ type: mongooseSchema.Types.ObjectId, ref: Vocabulary.name })
  @Type(() => Vocabulary)
  vocabulary: Vocabulary;

  @Prop({ type: mongooseSchema.Types.ObjectId, ref: User.name })
  @Type(() => User)
  user: User;

  @Prop({ type: Number, default: 0 })
  priorityPoint: number;

  @Prop({ type: Boolean, default: false })
  remembered: boolean;

  @Prop({ default: now() })
  createdAt: Date;

  @Prop({ default: now() })
  updatedAt: Date;
}

export const UserVocabSchema = SchemaFactory.createForClass(UserVocab);
