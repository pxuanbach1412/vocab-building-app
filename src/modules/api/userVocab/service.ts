import { Model } from 'mongoose';
import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { UserVocab, UserVocabDocument } from './schema';
import { NotFoundException } from '@nestjs/common';
import CreateUserVocabDto from './dto/create.dto';

@Injectable()
class UserVocabService {
  constructor(
    @InjectModel(UserVocab.name) private model: Model<UserVocabDocument>,
  ) {}

  async getAll() {
    return this.model.find();
  }

  async getOne(id: string) {
    const result = await this.model.findById(id);
    if (!result) {
      throw new NotFoundException();
    }
    return result;
  }

  create(data: CreateUserVocabDto) {
    const result = new this.model(data);
    return result.save();
  }

  async update(id: string, data) {
    const result = await this.model
      .findByIdAndUpdate(id, data)
      .setOptions({ overwrite: true, new: true });
    if (!result) {
      throw new NotFoundException();
    }
    return result;
  }

  async delete(id: string) {
    const result = await this.model.findByIdAndDelete(id);
    if (!result) {
      throw new NotFoundException();
    }
  }
}

export default UserVocabService;
