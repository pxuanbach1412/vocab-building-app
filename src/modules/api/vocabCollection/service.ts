import { Model } from 'mongoose';
import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { NotFoundException, BadRequestException } from '@nestjs/common';
import { VocabCollection, VocabCollectionDocument } from './schema';
import CreateVocabCollectionDto from './dto/create.dto';
import UpdateVocabCollectionDto from './dto/update.dto';
import QueryParamVocabCollection from './dto/query.dto';
import { CloudinaryService } from '../../../cloudinary/cloudinary.service';

@Injectable()
class VocabCollectionService {
  constructor(
    @InjectModel(VocabCollection.name)
    private model: Model<VocabCollectionDocument>,
    private cloudinary: CloudinaryService,
  ) {}

  async getAll(params: QueryParamVocabCollection) {
    const searchQuery = params.search
      ? {
          $or: [
            { name: { $regex: params.search, $options: 'i' } },
            { 'author.fullName': { $regex: params.search, $options: 'i' } },
          ],
        }
      : {};

    const isPrivateQuery =
      typeof params.isPrivate !== 'undefined' && params.isPrivate !== null
        ? {
            isPrivate: params.isPrivate,
          }
        : {};

    const authorQuery = params.author
      ? {
          author: params.author,
        }
      : {};

    const query = {
      $and: [searchQuery, isPrivateQuery, authorQuery],
    };

    return this.model
      .find(query)
      .populate('author')
      .skip(params.offset)
      .limit(params.limit);
  }

  async getOne(id: string) {
    const result = await this.model.findById(id);
    if (!result) {
      throw new NotFoundException();
    }
    return result;
  }

  async create(
    userId: string,
    data: CreateVocabCollectionDto,
    file?: Express.Multer.File,
  ) {
    let uploadedFile = null;
    if (file != null) {
      uploadedFile = await this.uploadImageToCloudinary(file);
    }
    const result = new this.model({
      ...data,
      author: userId,
      thumbnail: uploadedFile ? uploadedFile.secure_url : '',
      thumbnailPublicId: uploadedFile ? uploadedFile.public_id : '',
    });
    return result.save();
  }

  async uploadImageToCloudinary(file: Express.Multer.File) {
    return await this.cloudinary
      .uploadImage(file, VocabCollection.name)
      .catch((error) => {
        console.error('Error uploading image:', error);
        throw new BadRequestException();
      });
  }

  async removeImageToCloudinary(filePublicId: string) {
    return await this.cloudinary.removeImage(filePublicId).catch((error) => {
      console.error('Error removing image:', error);
      throw new BadRequestException();
    });
  }

  async update(id: string, data: UpdateVocabCollectionDto) {
    const result = await this.model
      .findByIdAndUpdate(id, data)
      .setOptions({ overwrite: true, new: true });
    if (!result) {
      throw new NotFoundException();
    }
    return result;
  }

  async delete(id: string) {
    const result = await this.getOne(id);
    const removedFile = await this.removeImageToCloudinary(
      result.thumbnailPublicId,
    );
    console.log(removedFile);
    await this.model.findByIdAndDelete(id);
  }
}

export default VocabCollectionService;
