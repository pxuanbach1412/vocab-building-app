import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import {
  HydratedDocument,
  Schema as mongooseSchema,
  ObjectId,
  now,
} from 'mongoose';
import { User } from '../user/schema';
import { Transform, Type } from 'class-transformer';
import { Field, ObjectType } from '@nestjs/graphql';

export type VocabCollectionDocument = HydratedDocument<VocabCollection>;

@ObjectType()
@Schema()
export class PreviewVocab {
  @Field({ nullable: true })
  @Prop({ type: String })
  pronunciation: string;

  @Field()
  @Prop({ type: String, required: true })
  word: string;

  @Field()
  @Prop({ type: String, required: true })
  shortMean: string;
}

export const PreviewVocabSchema = SchemaFactory.createForClass(PreviewVocab);

@ObjectType()
@Schema({ timestamps: true })
export class VocabCollection {
  @Field(() => String)
  @Transform(({ value }) => value.toString())
  @Type(() => String)
  _id: ObjectId;

  @Field()
  @Prop({ type: String, required: true })
  name: string;

  @Field({ nullable: true })
  @Prop({ type: String })
  description: string;

  @Field({ nullable: true })
  @Prop({ type: String })
  thumbnail: string;

  @Field({ nullable: true })
  @Prop({ type: String })
  thumbnailPublicId: string;

  @Field({ nullable: true })
  @Prop({ type: Boolean, default: true })
  isPrivate: boolean;

  @Prop({ type: [{ type: mongooseSchema.Types.ObjectId, ref: User.name }] })
  @Type(() => User)
  subscribers: User[];

  @Prop({ type: mongooseSchema.Types.ObjectId, ref: User.name })
  @Type(() => User)
  author: User;

  @Field((type) => [PreviewVocab], { nullable: true })
  @Prop({ _id: false, type: [PreviewVocabSchema], default: [] })
  previewVocabs: PreviewVocab[];

  @Field()
  @Prop({ default: now() })
  createdAt: Date;

  @Field()
  @Prop({ default: now() })
  updatedAt: Date;
}

export const VocabCollectionSchema =
  SchemaFactory.createForClass(VocabCollection);
