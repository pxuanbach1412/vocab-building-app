import { ApiProperty, ApiPropertyOptional } from '@nestjs/swagger';
import { IsArray, IsBoolean, IsNotEmpty, IsString } from 'class-validator';
import PreviewVocabDto from './previewVocab.dto';

class UpdateVocabCollectionDto {
  @ApiPropertyOptional()
  @IsString()
  name?: string;

  @ApiPropertyOptional()
  @IsString()
  description?: string;

  @ApiPropertyOptional()
  @IsBoolean()
  isPrivate?: boolean;

  @ApiPropertyOptional()
  @IsString()
  author?: string;

  @ApiPropertyOptional()
  @IsArray()
  previewVocabs?: PreviewVocabDto[];

  @ApiPropertyOptional()
  @IsArray()
  subscribers: string[];
}

export default UpdateVocabCollectionDto;
