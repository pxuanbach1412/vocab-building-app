import {
  IsString,
  IsNotEmpty,
  IsArray,
  IsNumber,
  IsMongoId,
  IsBoolean,
} from 'class-validator';
import { ApiProperty, ApiPropertyOptional } from '@nestjs/swagger';

class CreateVocabCollectionDto {
  @ApiProperty()
  @IsString()
  @IsNotEmpty()
  name: string;

  @ApiPropertyOptional()
  @IsString()
  description: string;

  @ApiProperty()
  @IsBoolean()
  @IsNotEmpty()
  isPrivate: boolean;

  @ApiPropertyOptional({ type: 'string', format: 'binary' })
  file: Express.Multer.File;
}

export default CreateVocabCollectionDto;
