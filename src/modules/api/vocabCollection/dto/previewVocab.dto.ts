import { ApiProperty } from '@nestjs/swagger';
import { IsString } from 'class-validator';

class PreviewVocabDto {
  @ApiProperty()
  @IsString()
  pronunciation: string;

  @ApiProperty()
  @IsString()
  word: string;

  @ApiProperty()
  @IsString()
  shortMean: string;
}

export default PreviewVocabDto;
