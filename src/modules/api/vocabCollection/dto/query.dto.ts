import { ApiProperty } from '@nestjs/swagger';
import { PaginationParams } from 'src/utils/paginationParams';

class QueryParamVocabCollection extends PaginationParams {
  @ApiProperty({ required: false })
  search?: string;

  @ApiProperty({ required: false })
  isPrivate?: boolean;

  @ApiProperty({ required: false })
  author?: string;
}

export default QueryParamVocabCollection;
