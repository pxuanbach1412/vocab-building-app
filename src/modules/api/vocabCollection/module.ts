import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { VocabCollection, VocabCollectionSchema } from './schema';
import VocabCollectionService from './service';
import VocabCollectionController from './controller';
import { CloudinaryModule } from '../../../cloudinary/cloudinary.module';

@Module({
  imports: [
    MongooseModule.forFeature([
      { name: VocabCollection.name, schema: VocabCollectionSchema },
    ]),
    CloudinaryModule,
  ],
  controllers: [VocabCollectionController],
  providers: [VocabCollectionService],
  exports: [],
})
export class VocabCollectionModule {}
