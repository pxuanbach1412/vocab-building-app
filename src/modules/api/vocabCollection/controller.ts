import {
  Body,
  Controller,
  Delete,
  Get,
  Param,
  Post,
  Put,
  UseInterceptors,
  UploadedFile,
  ParseFilePipeBuilder,
  HttpStatus,
  UseGuards,
  Req,
  Query,
} from '@nestjs/common';
import ParamsMongoId from '../../../utils/paramsWithId';
import {
  ApiTags,
  ApiParam,
  ApiConsumes,
  ApiBearerAuth,
  ApiProperty,
} from '@nestjs/swagger';
import CreateVocabCollectionDto from './dto/create.dto';
import UpdateVocabCollectionDto from './dto/update.dto';
import { FileInterceptor } from '@nestjs/platform-express';
import { JwtAuthGuard } from '../auth/jwtAuth.guard';
import RequestWithUser from '../auth/requestUser.interface';
import MongooseClassSerializerInterceptor from 'src/utils/mongooseClassSerializer.interceptor';
import { VocabCollection } from './schema';
import VocabCollectionService from './service';
import QueryParamVocabCollection from './dto/query.dto';

@UseInterceptors(MongooseClassSerializerInterceptor(VocabCollection))
@Controller('vocab-collections')
@ApiTags('vocabulary collections')
export default class VocabCollectionController {
  constructor(private readonly service: VocabCollectionService) {}

  @Get()
  async getAll(@Query() params: QueryParamVocabCollection) {
    return this.service.getAll(params);
  }

  @ApiParam({ name: 'id', required: true })
  @Get(':id')
  async getById(@Param() { id }: ParamsMongoId) {
    return this.service.getOne(id);
  }

  @Post()
  @UseGuards(JwtAuthGuard)
  @ApiBearerAuth('access-token')
  @ApiConsumes('multipart/form-data')
  @ApiProperty()
  @UseInterceptors(FileInterceptor('file'))
  async create(
    @Req() request: RequestWithUser,
    @Body() payload: CreateVocabCollectionDto,
    @UploadedFile(
      new ParseFilePipeBuilder()
        .addFileTypeValidator({
          fileType: 'image/*',
        })
        .addMaxSizeValidator({
          maxSize: 1000 * 1024,
        })
        .build({
          errorHttpStatusCode: HttpStatus.UNPROCESSABLE_ENTITY,
          fileIsRequired: false,
        }),
    )
    file?: Express.Multer.File,
  ) {
    return await this.service.create(
      request.user._id.toString(),
      payload,
      file,
    );
  }

  @ApiParam({ name: 'id', required: true })
  @Put(':id')
  async update(
    @Param() { id }: ParamsMongoId,
    @Body() payload: UpdateVocabCollectionDto,
  ) {
    return this.service.update(id, payload);
  }

  @ApiParam({ name: 'id', required: true })
  @Delete(':id')
  async delete(@Param() { id }: ParamsMongoId) {
    console.log(id);
    return await this.service.delete(id);
  }
}
