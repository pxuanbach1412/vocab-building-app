import { Inject } from '@nestjs/common';
import { Args, Query, Resolver } from '@nestjs/graphql';
import { Vocabulary } from 'src/modules/api/vocabulary/schema';
import VocabularyService from 'src/modules/api/vocabulary/service';

@Resolver((of) => Vocabulary)
export class VocabularyResolver {
  constructor(
    @Inject(VocabularyService) private vocabularyService: VocabularyService,
  ) {}

  @Query((returns) => Vocabulary, { nullable: true })
  async vocabulary(@Args('id') id: string) {
    return await this.vocabularyService.getOne(id);
  }
}
