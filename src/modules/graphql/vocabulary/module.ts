import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import {
  Vocabulary,
  VocabularySchema,
} from 'src/modules/api/vocabulary/schema';
import { VocabularyResolver } from './resolver';
import VocabularyService from 'src/modules/api/vocabulary/service';

@Module({
  imports: [
    MongooseModule.forFeature([
      { name: Vocabulary.name, schema: VocabularySchema },
    ]),
  ],
  controllers: [],
  providers: [VocabularyResolver, VocabularyService],
})
export class VocabularyGraphqlModule {}
